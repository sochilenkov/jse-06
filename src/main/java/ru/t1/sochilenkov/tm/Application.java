package ru.t1.sochilenkov.tm;

import ru.t1.sochilenkov.tm.constant.ArgumentConstant;
import ru.t1.sochilenkov.tm.constant.CommandConstant;
import ru.t1.sochilenkov.tm.util.TerminalUtil;

public class Application {

    public static void main(String[] args) {
        processArguments(args);
        processCommands();
    }

    private static void processCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            showWelcome();
            System.out.println("ENTER COMMAND: ");
            final String command = TerminalUtil.nextLine();
            processCommand(command);
        }
    }

    private static void processArguments(final String[] arguments) {
        if (arguments == null || arguments.length < 1) return;
        processArgument(arguments[0]);
        exit(0);
    }

    private static void exit(int code) {
        System.exit(code);
    }

    private static void processCommand(final String command) {
        switch (command) {
            case CommandConstant.VERSION:
                showVersion();
                break;
            case CommandConstant.HELP:
                showHelp();
                break;
            case CommandConstant.ABOUT:
                showAbout();
                break;
            case CommandConstant.EXIT:
                exit(0);
                break;
            default:
                showErrorCommand();
        }
    }

    private static void processArgument(final String argument) {
        switch (argument) {
            case ArgumentConstant.VERSION:
                showVersion();
                break;
            case ArgumentConstant.HELP:
                showHelp();
                break;
            case ArgumentConstant.ABOUT:
                showAbout();
                break;
            default:
                showErrorArgument();
        }
    }

    private static void showWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void showErrorArgument() {
        System.out.println("[ERROR]");
        System.out.println("Input program arguments are not correct");
        exit(1);
    }

    private static void showErrorCommand() {
        System.out.println("[ERROR]");
        System.out.println("Current command is not correct");
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.6.0");
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Nikita Sochilenkov");
        System.out.println("E-mail: nsochilenkov@t1-consulting.ru");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s, %s - Show version info\n", CommandConstant.VERSION, ArgumentConstant.VERSION);
        System.out.printf("%s, %s - Show developer info\n", CommandConstant.ABOUT, ArgumentConstant.ABOUT);
        System.out.printf("%s, %s - Show help info\n", CommandConstant.HELP, ArgumentConstant.HELP);
        System.out.printf("%s - Close application\n", CommandConstant.EXIT);
    }

}