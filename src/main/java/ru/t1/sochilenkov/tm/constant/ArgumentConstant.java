package ru.t1.sochilenkov.tm.constant;

public final class ArgumentConstant {

    public static final String HELP = "-h";

    public static final String VERSION = "-v";

    public static final String ABOUT = "-a";

    private ArgumentConstant() {
    }

}
